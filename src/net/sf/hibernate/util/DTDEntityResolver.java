//$Id: DTDEntityResolver.java,v 1.9 2005/01/26 21:59:33 oneovthafew Exp $
//Contributed by Markus Meissner
package net.sf.hibernate.util;

import java.io.InputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

public class DTDEntityResolver implements EntityResolver {
	
	Log log = LogFactory.getLog(DTDEntityResolver.class);
	
	private static final String[] URLS = {
            "http://hibernate.sourceforge.net/",
            "http://svn.atlassian.com/svn/public/atlassian/vendor/hibernate-2.1.8/"
    };

	public InputSource resolveEntity (String publicId, String systemId) {
        for (int i = 0; i < URLS.length; i++)
        {
            String url = URLS[i];
            if ( systemId!=null && systemId.startsWith(url) ) {
                log.debug("trying to locate " + systemId + " in classpath under net/sf/hibernate/");
                // Search for DTD

                int filenameStart = systemId.lastIndexOf('/') + 1;
                if (filenameStart > 0 && filenameStart < systemId.length())
                {
                    String path = "net/sf/hibernate/" + systemId.substring( filenameStart );
                    ClassLoader classLoader = getClass().getClassLoader();
                    InputStream dtdStream = classLoader==null ?
                            getClass().getResourceAsStream(path) :
                            classLoader.getResourceAsStream(path);

                    if (dtdStream != null)
                    {
                        log.debug("found " + systemId + " in classpath");
                        InputSource source = new InputSource(dtdStream);
                        source.setPublicId(publicId);
                        source.setSystemId(systemId);
                        return source;
                    }
                }

            }

        }

        // use the default behaviour
        return null;
	}
	
}







