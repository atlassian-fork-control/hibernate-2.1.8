//$Id: CacheFactory.java,v 1.6 2004/06/04 05:43:44 steveebersole Exp $
package net.sf.hibernate.cache;

import net.sf.hibernate.MappingException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Element;

/**
 * @author Gavin King
 */
public final class CacheFactory {
	
	private static final Log log = LogFactory.getLog(CacheFactory.class);
	
	private CacheFactory() {}
	
	public static final String READ_ONLY = "read-only";
	public static final String READ_WRITE = "read-write";
	public static final String NONSTRICT_READ_WRITE = "nonstrict-read-write";
	public static final String TRANSACTIONAL = "transactional";

	public static CacheConcurrencyStrategy createCache(Element node, String name, boolean mutable) 
	throws MappingException {
		
		String usage = node.attributeValue("usage");
		if (StringUtils.isNotEmpty(usage)) {
			return createCacheByUsage( usage, name, mutable );
		}

		String strategyClassName = node.attributeValue("strategy-class");
		if (StringUtils.isNotEmpty(strategyClassName)) {
			return createCacheByClassName( strategyClassName, name, mutable );
		}

		throw new MappingException(
				"You need to specify either a 'usage' or a 'strategy-class' attribute for a 'cache' element");
	}
	
	private static CacheConcurrencyStrategy createCacheByClassName(
			String strategyClassName, String name, boolean mutable) throws MappingException {
		
		try {
			Class strategyClass = Class.forName(strategyClassName);
			if (!CacheConcurrencyStrategy.class.isAssignableFrom(strategyClass)) {
				throw new MappingException("Class '" + strategyClassName
						+ "' needs to implement '"
						+ CacheConcurrencyStrategy.class.getName() + "'");
			}
			return (CacheConcurrencyStrategy) strategyClass.newInstance();
		} catch (ClassNotFoundException e) {
			throw new MappingException("Unable to instantiate class with name '" + strategyClassName + "'", e);
		} catch (InstantiationException e) {
			throw new MappingException("Unable to instantiate class with name '" + strategyClassName + "'", e);
		} catch (IllegalAccessException e) {
			throw new MappingException("Unable to instantiate class with name '" + strategyClassName + "'", e);
		}
	}

	private static CacheConcurrencyStrategy createCacheByUsage(String usage, String name, boolean mutable) 
	throws MappingException {
		
		if ( log.isDebugEnabled() ) log.debug("cache for: " + name + " usage strategy: " + usage);
		
		final CacheConcurrencyStrategy ccs;
		if ( usage.equals(READ_ONLY) ) {
			if (mutable) log.warn( "read-only cache configured for mutable: " + name );
			ccs = new ReadOnlyCache();
		}
		else if ( usage.equals(READ_WRITE) ) {
			ccs = new ReadWriteCache();
		}
		else if ( usage.equals(NONSTRICT_READ_WRITE) ) {
			ccs = new NonstrictReadWriteCache();
		}
		else if ( usage.equals(TRANSACTIONAL) ) {
			ccs = new TransactionalCache();
		}
		else {
			throw new MappingException("cache usage attribute should be read-write, read-only, nonstrict-read-write or transactional");
		}
		return ccs;
	}
		
}
