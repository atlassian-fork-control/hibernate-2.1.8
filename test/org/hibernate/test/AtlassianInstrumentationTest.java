package org.hibernate.test;

import junit.framework.TestCase;
import net.sf.hibernate.atlassian.AtlassianInstrumentation;

public class AtlassianInstrumentationTest extends TestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
    }

    public void testNoRegistrations()
    {
        AtlassianInstrumentation.startSplit("foo").stop();
    }

    public void testNoDuplicateRegistrations() throws Exception
    {
        MockAtlasSplitFactory atlasSplitFactory = new MockAtlasSplitFactory();

        AtlassianInstrumentation.registerFactory(atlasSplitFactory);
        AtlassianInstrumentation.registerFactory(atlasSplitFactory);

        AtlassianInstrumentation.startSplit("foo").stop();

        assertEquals(1, atlasSplitFactory.splitStartCount);

        AtlassianInstrumentation.unregisterFactory(atlasSplitFactory);
    }

    public void testMultipleRegistrations()
    {
        MockAtlasSplitFactory atlasSplitFactory1 = new MockAtlasSplitFactory();
        MockAtlasSplitFactory atlasSplitFactory2 = new MockAtlasSplitFactory();

        AtlassianInstrumentation.registerFactory(atlasSplitFactory1);
        AtlassianInstrumentation.registerFactory(atlasSplitFactory2);

        AtlassianInstrumentation.startSplit("foo").stop();

        assertEquals(1, atlasSplitFactory1.splitStartCount);
        assertEquals(1, atlasSplitFactory2.splitStartCount);
        assertEquals(1, atlasSplitFactory1.splitStopCount);
        assertEquals(1, atlasSplitFactory2.splitStopCount);

        AtlassianInstrumentation.unregisterFactory(atlasSplitFactory1);
        AtlassianInstrumentation.unregisterFactory(atlasSplitFactory2);
    }

    public void testUnregistering()
    {
        MockAtlasSplitFactory atlasSplitFactory = new MockAtlasSplitFactory();

        AtlassianInstrumentation.registerFactory(atlasSplitFactory);

        AtlassianInstrumentation.startSplit("foo").stop();

        AtlassianInstrumentation.unregisterFactory(atlasSplitFactory);

        AtlassianInstrumentation.startSplit("foo").stop();

        assertEquals(1, atlasSplitFactory.splitStartCount);
    }

    public void testRegisteringNullFactory()
    {
        AtlassianInstrumentation.registerFactory(null);
    }

    public void testUnregisteringNullFactory()
    {
        AtlassianInstrumentation.unregisterFactory(null);
    }

    private static class MockAtlasSplitFactory implements AtlassianInstrumentation.AtlasSplitFactory
    {
        int splitStartCount = 0;
        int splitStopCount = 0;

        @Override
        public AtlassianInstrumentation.AtlasSplit startSplit(String name)
        {
            splitStartCount++;
            return new AtlassianInstrumentation.AtlasSplit()
            {
                @Override
                public void stop()
                {
                    splitStopCount++;
                }
            };
        }
    }
}
